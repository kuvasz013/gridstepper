import React, { useEffect, useRef, useState } from 'react';
import './App.css';

const KEYDOWN = 40;
const KEYUP = 38;
const KEYLEFT = 37;
const KEYRIGHT = 39;
const MAX_MOVES = 5;
const N = 10;

function App() {

  const [pos, setPos] = useState([Math.round(Math.random() * N), Math.round(Math.random() * N)]);
  const posRef = useRef(pos);
  const history = useRef<any[]>();
  const moves = useRef<number>(0);

  const cells = [];

  const handleMove = (e: KeyboardEvent) => {
    if (moves.current >= MAX_MOVES) return;
    // Needs validation if we are off the board
    if (e.keyCode === KEYDOWN) {
      posRef.current = [posRef.current[0] + 1, posRef.current[1]]
      setPos(posRef.current);
    }
    else if (e.keyCode === KEYUP) {
      posRef.current = [posRef.current[0] - 1, posRef.current[1]]
      setPos(posRef.current);
    }
    else if (e.keyCode === KEYLEFT) {
      posRef.current = [posRef.current[0], posRef.current[1] - 1]
      setPos(posRef.current);
    }
    else if (e.keyCode === KEYRIGHT) {
      posRef.current = [posRef.current[0], posRef.current[1] + 1]
      setPos(posRef.current);
    }
    
    moves.current = moves.current + 1;
  }

  useEffect(() => {
    window.addEventListener('keydown', handleMove);
    // Cleanup caused the failure of the move listener, needs investigation
    //return window.removeEventListener('keydown', test);
  }, [])

  useEffect(() => {
    if (history.current) {
      history.current = [...history.current, posRef.current]
    } else {
      history.current = [posRef.current];
    }
  }, [pos])

  // Generating grid
  for (let i = 0; i < N; i++) {
    let row = [];
    for (let j = 0; j < N; j++) {
      row.push([i, j]);
    }
    cells.push(row);
  }

  return (
    <div className="App">
      <header className="App-header">
        <table>
          <tbody>
            {
              cells.map((rows, rowIndex) => {
                return (<tr key={rowIndex}>
                  {
                    rows.map((cell, cellIndex) => {
                      return (
                        // Coloring cell
                        cell[0] === pos[0] && cell[1] === pos[1]
                          ? <td key={cellIndex} className='current'></td>
                          : <td key={cellIndex}></td>
                      )
                    })
                  }
                </tr>)
              })
            }
          </tbody>
        </table>
        <span>{(moves.current + 1 >= MAX_MOVES) && `Moves: ${JSON.stringify(history.current)}`}</span>
      </header>
    </div>
  );
}

export default App;
